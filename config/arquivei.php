<?php

return [
    'url' => env('ARQUIVEI_URL'),
    'api_version' => env('ARQUIVEI_API_VERSION'),
    'api_id' => env('ARQUIVEI_API_ID'),
    'api_key' => env('ARQUIVEI_API_KEY'),
    'timeout' => env('ARQUIVEI_TIMEOUT'),
];