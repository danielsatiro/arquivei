<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Repositories;

use App\Models\Nfe;

/**
 * Description of NfeRepository
 *
 * @author daniel
 */
class NfeRepository extends Repository
{
    public function __construct()
    {
        parent::__construct(new Nfe());
    }

    public function insertNfes(array $data) : bool
    {
        foreach ($data as $value) {
            $this->create((array) $value);
        }

        return true;
    }
}
