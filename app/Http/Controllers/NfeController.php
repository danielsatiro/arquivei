<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use App\Repositories\NfeRepository;
use App\Service\ArquiveiService;
use Core\Dependencies\ApiClientException;
use Exception;
use Core\Modules\Nfe\Retrieve\UseCase as Retrieve;
use Core\Modules\Nfe\Show\UseCase as Show;
use Core\Adapters\LogAdapter;

class NfeController extends Controller
{
    private $nfeRepository;
    private $arquivei;
    private $logAdapter;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(NfeRepository $nfeRepository, ArquiveiService $arquivei, LogAdapter $logAdapter)
    {
        $this->nfeRepository = $nfeRepository;
        $this->arquivei = $arquivei;
        $this->logAdapter = $logAdapter;
    }

    /**
     * @OA\Get(
     *      path="/v1/nfe/received",
     *      operationId="nfeReceived",
     *      tags={"Nfe"},
     *      summary="To recover in Arquivei Nfe info and record in BD",
     *      description="Returns empty",
     *      @OA\Parameter(
     *          name="limit",
     *          description="The maximum amount of XMLs that will be returned",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="cursor",
     *          description="The cursor position which the search will start from. This value is received after a successfull request on the body of the response as next.",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="integer"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="access_key",
     *          description="Filter by a list of access_key. Example using this filter = v1/nfe/received?access_key[]=44charKey",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Parameter(
     *          name="filter",
     *          description="Filter by presence or value of custom info using Arquivei's filter language",
     *          required=false,
     *          in="query",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *     @OA\Response(
     *         response=400,
     *         description="Something wrong happened"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal Server Error"
     *     ),
     *     security={
     *         {"passaport": {}}
     *     }
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $useCase = new Retrieve($this->logAdapter, $this->nfeRepository, $this->arquivei);
        $useCase->execute($request->all());

        $response = [
            'status' => $useCase->getResponse()->getStatus()
        ];

        return response()->json($response, $response['status']['code']);
    }

    /**
     * @OA\Get(
     *      path="/v1/nfe/received/{accessKey}",
     *      operationId="nfeReceivedByAccessKey",
     *      tags={"Nfe"},
     *      summary="To recover in local DB Nfe info by access key",
     *      description="Returns Nfe info",
     *      @OA\Parameter(
     *          name="accessKey",
     *          description="Filter by a list of access_key.",
     *          required=true,
     *          in="path",
     *          @OA\Schema(
     *              type="string"
     *          )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation"
     *       ),
     *     @OA\Response(
     *         response=400,
     *         description="Something wrong happened"
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Not found"
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal Server Error"
     *     ),
     *     security={
     *         {"passaport": {}}
     *     }
     * )
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $accessKey)
    {
        $useCase = new Show($this->logAdapter, $this->nfeRepository);
        $useCase->execute($accessKey);

        $response = [
            'status' => $useCase->getResponse()->getStatus(),
            'data' => $useCase->getResponse()->getNfeEntity()->getXml()
        ];

        return response()->json($response, $response['status']['code']);
    }
}
