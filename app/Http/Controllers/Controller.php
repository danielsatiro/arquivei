<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
    * @OA\Info(
    *      version="1.0.0",
    *      title="Desafio Bolton",
    *      description="Test for Backend Developer at Arquivei",
    *      @OA\Contact(
    *          email="danielsatiro2003@yahoo.com.br"
    *      ),
    *     @OA\License(
    *         name="Apache 2.0",
    *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
    *     )
    * )
    */
}
