<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Core\Dependencies\ApiClientException;
use Core\Dependencies\ArquiveiServiceInterface;

/**
 * Description of ArquiveiService
 *
 * @author daniel
 */
class ArquiveiService implements ArquiveiServiceInterface
{
    private $http;

    public function __construct()
    {
        $this->http = new Client([
            'base_uri' => config('arquivei.url'),
            'timeout' => config('arquivei.timeout', 30),
            'headers' => [
                "Content-Type" => "application/json",
                'x-api-id' => config('arquivei.api_id'),
                'x-api-key' => config('arquivei.api_key'),
            ],
        ]);
    }

    public function retrieveNfeReceived(array $params = [])
    {
        $endpoint = config('arquivei.api_version') . '/nfe/received';
        try {
            $response = $this->http->get($endpoint, ['query' => $params]);

            return json_decode($response->getBody()->getContents());
        } catch (RequestException $e) {
            $response = $e->getResponse()->getBody()->getContents();
            Log::error(sprintf('Endpoint: %s|Params: %s|Response: %s',
                $endpoint, json_encode($params), $response));

            ApiClientException::throwCustomMessage($response, '(Arquivei) Error on retrieve Nfe received', $e->getResponse()->getStatusCode());
        }
    }
}
