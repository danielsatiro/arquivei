<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Description of Nfe
 *
 * @author daniel
 */
class Nfe extends Model
{
    protected $table = 'nfes';
    protected $primaryKey = 'id';

    protected $fillable = [
        'access_key', 'xml',
    ];

    protected $hidden = [
        'created_at', 'updated_at',
    ];
}
