CREATE DATABASE IF NOT EXISTS `backend`;

DROP TABLE IF EXISTS `backend`.`migrations`;
CREATE TABLE `backend`.`migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `backend`.`nfes`;
CREATE TABLE `backend`.`nfes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `access_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `xml` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nfes_access_key_unique` (`access_key`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO `backend`.`migrations`
(`id`,`migration`,`batch`)
VALUES
('3', '2019_07_20_173240_create_nfes_table', '1');


CREATE DATABASE IF NOT EXISTS `backend_test`;

DROP TABLE IF EXISTS `backend_test`.`migrations`;
CREATE TABLE `backend_test`.`migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `backend_test`.`nfes`;
CREATE TABLE `backend_test`.`nfes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `access_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `xml` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `nfes_access_key_unique` (`access_key`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


INSERT INTO `backend_test`.`migrations`
(`id`,`migration`,`batch`)
VALUES
('3', '2019_07_20_173240_create_nfes_table', '1');