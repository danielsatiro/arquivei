<?php

use Faker\Generator as Faker;

$factory->define(\App\Models\Nfe::class, function (Faker $faker) {
    $xml = new SimpleXMLElement('<root/>');
    $fackeData = $faker->sentences();
    array_walk_recursive($fackeData, array ($xml, 'addChild'));
    return [
        'access_key' => $faker->word,
        'xml' => base64_encode($xml->asXML()),
    ];
});
