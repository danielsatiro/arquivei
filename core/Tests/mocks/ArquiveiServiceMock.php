<?php

namespace Tests\Mocks;

use Core\Dependencies\ArquiveiServiceInterface;
use Core\Dependencies\ApiClientException;
/**
 * Description of ArquiveiServiceMock
 *
 * @author daniel
 */
class ArquiveiServiceMock implements ArquiveiServiceInterface
{
    const BAD_REQUEST = '3518010471014900011555001000008403100008403';

    public function retrieveNfeReceived(array $params = [])
    {
        if (!empty($params['access_key']) && $params['access_key'] == self::BAD_REQUEST) {
            $response = '{"status":{"code":400,"message":"Bad Request"}}';
            ApiClientException::throwCustomMessage($response, '(Arquivei) Error on retrieve Nfe received', 400);
        }

        return (object) array (
            'status' =>
            array (
              'code' => 200,
              'message' => 'Ok',
            ),
            'data' =>
            array (
              0 =>
              array (
                'access_key' => '123987419285081409182945127340172',
                'xml' => 'PD94bWwgdmVyc2lvbj0iMS4wIj4KPGZpbG1lcz4KICAgIDxmaWxtZSBpZD0iMSI+CiAgICAgICAgPHRpdHVsbz5PIFhNTCB2ZXN0ZSBwcmFkYTwvdGl0dWxvPgogICAgICAgIDxyZXN1bW8+TyBmaWxtZSBtb3N0cmEgYSBlbGVnw6JuY2lhIGRhIFhNTCBuYSByZXByZXNlbnRhw6fDo28gZGUgZGFkb3MgZXN0cnV0dXJhZG9zIGUgc2VtaSBlc3RydXR1cmFkb3MuPC9yZXN1bW8+CiAgICAgICAgPGdlbmVybz5BdmVudHVyYTwvZ2VuZXJvPgogICAgICAgIDxnZW5lcm8+RG9jdW1lbnTDoXJpbzwvZ2VuZXJvPgogICAgICAgIDxlbGVuY28+CiAgICAgICAgICAgIDxhdG9yPk1hcmsgVVBsYW5ndWFnZTwvYXRvcj4KICAgICAgICAgICAgPGF0b3I+TWFyeSB3ZWxsLUZvcm1lZDwvYXRvcj4KICAgICAgICAgICAgPGF0b3I+U2VkbmEgRC4gQXRhYmFzZTwvYXRvcj4KICAgICAgICA8L2VsZW5jbz4KICAgIDwvZmlsbWU+CjwvZmlsbWVzPg==',
              ),
              1 =>
              array (
                'access_key' => '123987419285081409182945127354321',
                'xml' => 'PD94bWwgdmVyc2lvbj0iMS4wIj4KPGZpbG1lcz4KICAgIDxmaWxtZSBpZD0iMSI+CiAgICAgICAgPHRpdHVsbz5PIFhNTCB2ZXN0ZSBwcmFkYTwvdGl0dWxvPgogICAgICAgIDxyZXN1bW8+TyBmaWxtZSBtb3N0cmEgYSBlbGVnw6JuY2lhIGRhIFhNTCBuYSByZXByZXNlbnRhw6fDo28gZGUgZGFkb3MgZXN0cnV0dXJhZG9zIGUgc2VtaSBlc3RydXR1cmFkb3MuPC9yZXN1bW8+CiAgICAgICAgPGdlbmVybz5BdmVudHVyYTwvZ2VuZXJvPgogICAgICAgIDxnZW5lcm8+RG9jdW1lbnTDoXJpbzwvZ2VuZXJvPgogICAgICAgIDxlbGVuY28+CiAgICAgICAgICAgIDxhdG9yPk1hcmsgVVBsYW5ndWFnZTwvYXRvcj4KICAgICAgICAgICAgPGF0b3I+TWFyeSB3ZWxsLUZvcm1lZDwvYXRvcj4KICAgICAgICAgICAgPGF0b3I+U2VkbmEgRC4gQXRhYmFzZTwvYXRvcj4KICAgICAgICA8L2VsZW5jbz4KICAgIDwvZmlsbWU+CjwvZmlsbWVzPg==',
              ),
              2 =>
              array (
                'access_key' => '123987419285081409182945127312345',
                'xml' => 'PD94bWwgdmVyc2lvbj0iMS4wIj4KPGZpbG1lcz4KICAgIDxmaWxtZSBpZD0iMSI+CiAgICAgICAgPHRpdHVsbz5PIFhNTCB2ZXN0ZSBwcmFkYTwvdGl0dWxvPgogICAgICAgIDxyZXN1bW8+TyBmaWxtZSBtb3N0cmEgYSBlbGVnw6JuY2lhIGRhIFhNTCBuYSByZXByZXNlbnRhw6fDo28gZGUgZGFkb3MgZXN0cnV0dXJhZG9zIGUgc2VtaSBlc3RydXR1cmFkb3MuPC9yZXN1bW8+CiAgICAgICAgPGdlbmVybz5BdmVudHVyYTwvZ2VuZXJvPgogICAgICAgIDxnZW5lcm8+RG9jdW1lbnTDoXJpbzwvZ2VuZXJvPgogICAgICAgIDxlbGVuY28+CiAgICAgICAgICAgIDxhdG9yPk1hcmsgVVBsYW5ndWFnZTwvYXRvcj4KICAgICAgICAgICAgPGF0b3I+TWFyeSB3ZWxsLUZvcm1lZDwvYXRvcj4KICAgICAgICAgICAgPGF0b3I+U2VkbmEgRC4gQXRhYmFzZTwvYXRvcj4KICAgICAgICA8L2VsZW5jbz4KICAgIDwvZmlsbWU+CjwvZmlsbWVzPg==',
              ),
            ),
          );
    }
}
