<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Tests\Mocks;

use App\Repositories\RepositoryInterface;

/**
 * Description of NfeRepositoryMock
 *
 * @author danie
 */
class NfeRepositoryMock implements RepositoryInterface
{
    private $value;
    private static $created = 0;

    const BAD_REQUEST = '3518010471014900011555001000008403100008403';

    public function all()
    {
        return;
    }

    public function create(array $data)
    {
        return;
    }

    public function update(array $data, $id)
    {
        return;
    }

    public function delete($id)
    {
        return;
    }

    public function show($id)
    {
        return;
    }

    public function insertNfes(array $data) : bool
    {
        self::$created++;

        if (self::$created == 2) {
            throw new \Exception('Integrity constraint violation: 1062', 23000);
        }

        return true;
    }

    public function getModel()
    {
        return $this;
    }

    public function where($column, $value)
    {
        $this->value = $value;
        return $this;
    }

    public function first()
    {
        if ($this->value == self::BAD_REQUEST)
        {
            return null;
        }
        return factory(\App\Models\Nfe::class)->create();
    }
}
