<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Modules\Entities;

/**
 * Description of Nfe
 *
 * @author daniel
 */
class Nfe
{
    private $id;
    private $accessKey;
    private $xml;

    public function __construct(int $id, string $accessKey, string $xml)
    {
        $this->id = $id;
        $this->accessKey = $accessKey;
        $this->xml = $xml;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAccessKey(): string
    {
        return $this->accessKey;
    }

    public function getXml(): string
    {
        return empty($this->xml) ? '' : base64_decode($this->xml);
    }


}
