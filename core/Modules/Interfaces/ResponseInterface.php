<?php

namespace Core\Modules\Interfaces;

interface ResponseInterface
{
    public function getStatus(): array;
}
