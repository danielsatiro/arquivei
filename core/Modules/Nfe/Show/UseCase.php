<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Modules\Nfe\Show;

use Core\Dependencies\LogInterface;
use App\Repositories\RepositoryInterface;
use Core\Modules\Nfe\Show\Responses\Response;
use Core\Modules\Entities\Nfe;
use \Core\Modules\Interfaces\ResponseInterface;

/**
 * Description of UseCase
 *
 * @author daniel
 */
class UseCase
{
    private $logger;
    private $nfeRepository;
    private $response;

    public function __construct(
            LogInterface $logger,
            RepositoryInterface $nfeRepository)
    {
        $this->logger = $logger;
        $this->nfeRepository = $nfeRepository;
    }

    public function execute($accessKey) : void
    {
        $nfe = $this->nfeRepository->getModel()
                ->where('access_key', $accessKey)->first();

        if (empty($nfe)) {
            $this->logger->info("Access Key not found {$accessKey}");

            $this->response = new Response(
                    new Nfe(0, '', ''),
                    404, 'Not Found');
            return;
        }
        
        $this->response = new Response(
                new Nfe($nfe->id, $nfe->access_key, $nfe->xml));
    }

    public function getResponse() : ResponseInterface
    {
        return $this->response;
    }
}
