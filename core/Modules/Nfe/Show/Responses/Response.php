<?php

namespace Core\Modules\Nfe\Show\Responses;

use Core\Modules\Interfaces\ResponseInterface;
use Core\Modules\Entities\Nfe;

class Response implements ResponseInterface
{
    private $nfeEntity;
    private $code;
    private $message;

    public function __construct(Nfe $nfeEntity, int $code = 200, string $message = 'Ok')
    {
        $this->nfeEntity = $nfeEntity;
        $this->code = $code;
        $this->message = $message;
    }

    public function getStatus(): array
    {
        return [
            'code' => $this->code,
            'message' => $this->message,
        ];
    }

    public function getNfeEntity(): Nfe
    {
        return $this->nfeEntity;
    }
}
