<?php

namespace Core\Modules\Nfe\Retrieve\Responses;

use Core\Modules\Interfaces\ResponseInterface;

class Response implements ResponseInterface
{
    private $code;
    private $message;

    public function __construct(int $code = 200, string $message = 'Ok')
    {
        $this->code = $code;
        $this->message = $message;
    }

    public function getStatus(): array
    {
        return [
            'code' => $this->code,
            'message' => $this->message,
        ];
    }
}
