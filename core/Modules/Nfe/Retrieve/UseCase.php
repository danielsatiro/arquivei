<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Modules\Nfe\Retrieve;

use Core\Dependencies\LogInterface;
use App\Repositories\RepositoryInterface;
use Core\Dependencies\ArquiveiServiceInterface;
use Core\Dependencies\ApiClientException;
use Core\Modules\Interfaces\ResponseInterface;
use Core\Modules\Nfe\Retrieve\Responses\Response;

/**
 * Description of UseCase
 *
 * @author daniel
 */
class UseCase
{
    private $logger;
    private $arquiveiServiceInterface;
    private $nfeRepository;
    private $response;

    public function __construct(
            LogInterface $logger,
            RepositoryInterface $nfeRepository,
            ArquiveiServiceInterface $arquiveiServiceInterface)
    {
        $this->logger = $logger;
        $this->nfeRepository = $nfeRepository;
        $this->arquiveiServiceInterface = $arquiveiServiceInterface;
    }

    public function execute(array $params = []) : void
    {
        $code = 200;
        $message = 'Ok';
        try {
            $nfes = $this->arquiveiServiceInterface->retrieveNfeReceived($params);
            $this->nfeRepository->insertNfes($nfes->data);
        } catch (ApiClientException $exc) {
            $this->logger->error("Error Arquivei Service {$exc->getCode()}: {$exc->getMessage()}");
            $code = $exc->getCode();
            $message = $exc->getMessage();
        } catch (\Exception $exc) {
            $this->logger->error("Error {$exc->getCode()}: {$exc->getMessage()}");
            $code = 500;
            $message = $exc->getMessage();
            if($exc->getCode() == 23000) {
                $code = 422;
                $message = 'Data already processed';
            }
        }

        $this->response = new Response($code, $message);
    }

    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }
}
