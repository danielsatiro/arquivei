<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Dependencies;

/**
 * Description of ApiClientExcepetion
 *
 * @author danie
 */
class ApiClientException extends \Exception
{
    public function __construct($message = "", $code = 0, $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public static function throwCustomMessage($responseBody, $defaultMessage = '', $defaultCode = 400)
    {
        $response = json_decode($responseBody, true);

        $message = !empty($defaultMessage) ? $defaultMessage :
                'Something unexpected happened. Try again.';

        $code = $defaultCode;

        if (!isset($response['error'])) {
            $code = $response['status']['code'] ?? $code;
            $message = $response['status']['message'] ?? $defaultMessage;
        }

        throw new ApiClientException($message, $code);
    }
}
