<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Adapters;

use Core\Dependencies\LogInterface;
use Illuminate\Support\Facades\Log;

/**
 * Description of LogAdapter
 *
 * @author daniel
 */
class LogAdapter implements LogInterface
{
    private $logger;

    public function __construct()
    {
        $this->logger = Log::getFacadeRoot();
    }

    public function error(string $message, array $context = []): void
    {
        $this->logger->error($message, $context);
    }

    public function info(string $message, array $context = []): void
    {
        $this->logger->info($message, $context);
    }
}
