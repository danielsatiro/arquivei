# Lumen PHP Framework

**Comandos de execução da App**
```
cd arquivei
cp .env.example .env
docker run --rm -v $(pwd):/app prooph/composer:7.2 install
docker-compose up -d
```

Caso necessário alterar as variáveis `ARQUIVEI_` no arquivo `.env` na raiz do projeto

Endereço para acessar a aplicação: http://localhost

Endereço para documentação: http://localhost/api/documentation

**Rotas:**
```
GET: '/v1/nfe/received'
GET: '/v1/nfe/received/{accessKey}'
```

**Testes:**

Dentro do arquivei_php_1 executar o comando
```
./vendor/bin/phpunit
```
Relatório de cobertura gerado em:
```
./arquivei/report/html/app/index.html
```