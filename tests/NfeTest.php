<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

use Core\Modules\Nfe\Show\UseCase as Show;
use Core\Modules\Nfe\Retrieve\UseCase as Retrieve;
use Core\Adapters\LogAdapter;
use Tests\Mocks\NfeRepositoryMock;
use Tests\Mocks\ArquiveiServiceMock;

class NfeTest extends TestCase
{
    use DatabaseTransactions;

    public function testReceived()
    {
        $this->get('v1/nfe/received');

        $this->assertEquals(200, $this->response->getStatusCode());
    }

    public function testReceivedAlreadyProcessed()
    {
        $useCase = new Retrieve(new LogAdapter(), new NfeRepositoryMock(), new ArquiveiServiceMock());
        $useCase->execute();
        $useCase->execute();

        $this->assertEquals(422, $useCase->getResponse()->getStatus()['code']);
    }

    public function testReceivedBadRequest()
    {
        $useCase = new Retrieve(new LogAdapter(), new NfeRepositoryMock(), new ArquiveiServiceMock());
        $useCase->execute(['access_key' => '3518010471014900011555001000008403100008403']);

        $this->assertEquals(400, $useCase->getResponse()->getStatus()['code']);
    }

    public function testReceivedByAccessKey()
    {
        $nfeMock = factory(\App\Models\Nfe::class)->create();
        $response = $this->json('GET', "v1/nfe/received/{$nfeMock->access_key}", []);

        $response->seeJson();
        $response->assertResponseStatus(200);
    }

    public function testReceivedByAccessKeyNotFound()
    {
        $response = $this->json('GET', 'v1/nfe/received/000000000000000000000000000000', []);

        $response->seeJson();
        $response->assertResponseStatus(404);
    }
}
